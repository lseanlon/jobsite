var cheerio = require('cheerio');
var fs = require('fs');

var contents = fs.readFileSync('./dist/step1category.html', 'utf8');


var categoryList = [];
var categoryUrlList = [];
var $ = cheerio.load(contents)
var $categoryListLink = $("[title='Genre']").next('.sub-container').find('a')

$categoryListLink.each(function(i, element) {
    var rowUrl = { 'url': $(element).prop('href'), 'title': $(element).html() };

    categoryList.push(rowUrl);
    categoryUrlList.push($(element).prop('href'));
});

console.log('Parse Genre into json...', categoryList);

// //write to file 
var timestamp = new Date().getTime().toString();
// var filePathName =  __dirname  + "/dist/data-categorylist-"+ timestamp+".txt";
var filePathName = __dirname + "/dist/data-categorylist.txt";


fs.appendFile(filePathName, JSON.stringify(categoryList), function(err) {
    if (err) {
        return console.log(err);
    }

    console.log("The file was saved!");
});
